package com.tapia.construccionesindustriales.practicasocketio1;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class SocketBackgroundService extends Service {
private Context thisContext = this;
Socket socket;
@Override
public void onCreate()
{

}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast toast1 =
                Toast.makeText(thisContext,
                        "Bienvenido", Toast.LENGTH_SHORT);

        toast1.show();
        try {
            socket = IO.socket("http://192.168.1.204:8080");
            socket.connect();
            //socket.emit("new-message", Nickname);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        message(socket);

    return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected void message (Socket socket)
    {
        socket.on("message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray data = (JSONArray) args[0];
                try {
                  ArrayList  MessageList = new ArrayList<>();
                    for( int i=0;data.length()>i; i++ ) {

                        System.out.println(data.length());
                        JSONObject jsonObject = data.getJSONObject(i);
                        String nickname = jsonObject.getString("author");
                        String message = jsonObject.getString("text");

                        // make instance of message

                        Message m = new Message(nickname+": ", message);


                        //add the message to the messageList

                        MessageList.add(m);

                        NotificationCompat.Builder mBuilder;
                        NotificationManager mNotifyMgr =(NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

                        int icono = R.mipmap.ic_launcher;
                        Intent intent = new Intent(thisContext, MainActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(thisContext, 0, intent, 0);

                        mBuilder =new NotificationCompat.Builder(getApplicationContext())
                                .setContentIntent(pendingIntent)
                                .setSmallIcon(icono)
                                .setContentTitle("Nuevo Mensaje de: " + jsonObject.getString("author"))
                                .setContentText(m.getMessage())
                                .setVibrate(new long[] {100, 250, 100, 500})
                                .setAutoCancel(true);



                        mNotifyMgr.notify(1, mBuilder.build());

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
