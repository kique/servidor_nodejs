package com.tapia.construccionesindustriales.practicasocketio1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Monitor extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, SocketBackgroundService.class);
        context.startService(service);
    }
}
