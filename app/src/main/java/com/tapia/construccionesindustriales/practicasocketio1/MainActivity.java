package com.tapia.construccionesindustriales.practicasocketio1;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Socket socket;
    private String Nickname ;
    public RecyclerView myRecylerView ;
    public List<Message> MessageList ;
    public ChatBoxAdapter chatBoxAdapter;
    public EditText messagetxt,usertxt ;
    public Button send ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this,SocketBackgroundService.class));
    usertxt= findViewById(R.id.txtUsuario);
    Nickname=usertxt.getText().toString();
    messagetxt = findViewById(R.id.txtMensaje);
    send = findViewById(R.id.btn);
    try {
        socket = IO.socket("http://192.168.1.204:8080");
        socket.connect();
        //socket.emit("new-message", Nickname);
    } catch (URISyntaxException e) {
        e.printStackTrace();
    }
        MessageList = new ArrayList<>();
        myRecylerView = (RecyclerView) findViewById(R.id.messagelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());

       send.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               System.out.println("User=> "+Nickname);
               socket.emit("messagedetection",usertxt.getText().toString(),messagetxt.getText().toString());

               messagetxt.setText(" ");
               message(socket);
           }
       });
message(socket);
        socket.on("userjoinedthechat", new Emitter.Listener() {
            @Override public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        String data = (String) args[0];

                        Toast.makeText(MainActivity.this,data,Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

        socket.on("userdisconnect", new Emitter.Listener() {
            @Override public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        String data = (String) args[0];

                        Toast.makeText(MainActivity.this,data,Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

//        socket.on("message", new Emitter.Listener() {
//            @Override public void call(final Object... args) {
//                runOnUiThread(new Runnable() {
//                    @Override public void run() {
//                        JSONArray data = (JSONArray) args[0];
//                        try {
//                            for ( int i=0;data.length()>i;i++ ) {
//
//
//                                JSONObject jsonObject = data.getJSONObject(i);
//                                String nickname = jsonObject.getString("author");
//                                String message = jsonObject.getString("text");
//
//                                // make instance of message
//
//                                Message m = new Message(nickname, message);
//
//
//                                //add the message to the messageList
//
//                                MessageList.add(m);
//
//                                // add the new updated list to the dapter
//                                chatBoxAdapter = new ChatBoxAdapter(MessageList);
//
//                                // notify the adapter to update the recycler view
//
//                                chatBoxAdapter.notifyDataSetChanged();
//
//                                //set the adapter for the recycler view
//
//                                myRecylerView.setAdapter(chatBoxAdapter);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                });
//            }
//        });

    }

    @Override

    protected void onDestroy() {
        socket.disconnect();
        startService(new Intent(this,SocketBackgroundService.class));
        super.onDestroy();

    }
    protected void message(Socket socket)
    {
        socket.on("message", new Emitter.Listener() {
            @Override public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override public void run() {
                        JSONArray data = (JSONArray) args[0];
                        try {
                            MessageList = new ArrayList<>();
                            for( int i=0;data.length()>i; i++ ) {

                                System.out.println(data.length());
                                JSONObject jsonObject = data.getJSONObject(i);
                                String nickname = jsonObject.getString("author");
                                String message = jsonObject.getString("text");

                                // make instance of message

                                Message m = new Message(nickname+": ", message);


                                //add the message to the messageList

                                MessageList.add(m);

                                // add the new updated list to the dapter
                                chatBoxAdapter = new ChatBoxAdapter(MessageList);

                                // notify the adapter to update the recycler view

                                chatBoxAdapter.notifyDataSetChanged();

                                //set the adapter for the recycler view

                                myRecylerView.setAdapter(chatBoxAdapter);
                                System.out.println("Esta es i=> "+i);



//                                NotificationCompat.Builder mBuilder;
//                                NotificationManager mNotifyMgr =(NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
//
//                                int icono = R.mipmap.ic_launcher;
//                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
//                                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);
//
//                                mBuilder =new NotificationCompat.Builder(getApplicationContext())
//                                        .setContentIntent(pendingIntent)
//                                        .setSmallIcon(icono)
//                                        .setContentTitle("Nuevo Mensaje de: " + jsonObject.getString("author"))
//                                        .setContentText(m.getMessage())
//                                        .setVibrate(new long[] {100, 250, 100, 500})
//                                        .setAutoCancel(true);
//
//
//
//                                mNotifyMgr.notify(1, mBuilder.build());

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        });
    }
}
