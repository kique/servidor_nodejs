var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var messages =[{
    id:1,
    text:"Bienvenido",
    author: "Boot"
}];

app.use(express.static('public'));

app.get('/hello', function(req, res){
    res.status(200).send('hi world');
});

io.on('connection', function(socket){
    console.log('Alguien se ha conectado');
    socket.emit('message', messages);

    socket.on('new-message', function(data){
        console.log("new message: "+data);
        messages.push(data);
        console.log(messages);
        io.sockets.emit('message', messages);
    });

    socket.on('messagedetection', (senderNickname,messageContent) => {
       
        //log the message in console 
 
        console.log(senderNickname+" :" +messageContent)
         //create a message object 
        let  message = {"text":messageContent, "author":senderNickname}
        messages.push(message);
           // send the message to the client side  
        io.sockets.emit('message', messages );
      
       });
});

server.listen(8080, function(){
    console.log('server run in port: 8080');
});